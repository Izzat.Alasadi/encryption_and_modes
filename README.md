# Encryption_and_modes

    oblig-3 inf143


## Problem 1.
    Consider a very simple block cipher implemented using the Gold function x3 on 32 bits: if the 32-bit plaintext is P and the 32-bit key is K, then the ciphertext is K3 ⊕P, where K3 is computed in the finite field with primitive polynomial x32 + x15 + x9 + x7 + x4 + x3 + 1, and ⊕ denotes the XOR operation. An implementation of this cipher is available in gold.py.
    Write an extension of this block cipher that can be used on an input of arbitrary length, i.e. not only 32 bits. If the input is not a multiple of 32, pad it with 0’s on the right. Implement the following modes of operation:
    • ECB;
    • CBC;
    • OFB.
    Encrypt the plaintext given in gold plaintext.in using each of the three modes of operation. Use K = 0101 . . . 01 (32 alternating zeros and ones) as the key.


## Problem 2
    Use the same block cipher described above with the Matyas- Meyer-Oseas construction to construct a hash function; you can use a vector of all 1’s for the first round key h0. What is the output size of this hash function? How many hashes would an attacker have to compute in order to find a pair of inputs with the same hash (in other words, what would the complexity of the so-called birthday attack be)? Hash the plaintext in gold plaintext.in using the resulting hash function.

## Problem 3 
    Recall that a cryptographically secure hash function h with output length l can be used to construct a block cipher as follows:
    • split the message (file) to be encoded into blocks M1, M2, . . . , MK of l bits;
    • pad MK if necessary;
    • choose an initialization vector IV and set M0 = IV ;
    • to encrypt block Mi, compute h(K||Mi−1) and XOR it with Mi (where K is the secret key).
    In this exercise, you will use the SHA-256 hash function to create an encryp- tion algorithm using the above approach. Implementations of SHA-256 should be easily accessible on all operating systems, e.g. the sha256sum command should be available on most Unix distributions. Note that the output of SHA-256 is always 256 bits long.
    Write a program which accepts a 256-bit initialization vector and a secret key of arbitrary length, and encrypts or decrypts a given file using the algorithm described above. Taking IV = 111...1 and K = 0101...01, use the program to encrypt the file gold plaintext.in.
