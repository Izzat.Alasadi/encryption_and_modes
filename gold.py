
import binascii
from dataclasses import dataclass
from hashlib import sha256
from math import ceil


class Gold:

    def __init__(self, file_name, irr=[1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], block_size=32, init_vector=[1]*32, init_key=[0, 1]*16) -> None:
        self.irr = irr
        self.BLOCK_SIZE = block_size
        self.file_name = file_name
        self.INIT_VEC = init_vector
        self.KEY = init_key

    def xor(self, v1, v2):
        result = []
        for i in range(len(v1)):
            b = (v1[i] + v2[i]) % 2
            result.append(b)
        return result

    def multiplication(self, A, B, irr):
        result = []
        for i in range(len(A)):
            result.append(0)

        for i in range(len(A)):
            if B[i] == 1:
                shift = A
                for _ in range(i):
                    do_we_have_overflow = (shift[-1] == 1)
                    shift = [0] + shift[:-1]
                    if do_we_have_overflow:
                        shift = self.xor(shift, irr)
                result = self.xor(result, shift)
        return result

    def gold(self, B):
        return self.multiplication(B, self.multiplication(B, B, self.irr), self.irr)

    # input: B: block, K: Key
    #output: xor( enc(block) , Key )
    def encrypt(self, B, K):

        # this case satisfies iff the size of the block is less than in our case 32-bits
        # and the condition will be trigger for the last block with size less than 32
        assert len(B) == 32
        assert len(K) == 32

        return self.xor(self.gold(B), K)

    # padding the rest bits with zeros to the right of original data
    def pad_bits(self, data):
        return data + [0] * (self.BLOCK_SIZE - len(data) % self.BLOCK_SIZE)

    # Electronic code book

    def ECB(self, file, block_enc):
        cipher = []

        # break the plaintext into blocks
        # blocks: number of the output's cipher blocks= (plaintext/ block_size) /iterations of encryption

        for i in range(ceil(len(file) / self.BLOCK_SIZE + 1)):
            start = i * self.BLOCK_SIZE
            if start >= len(file):
                break
            end = min(len(file), (i+1) * self.BLOCK_SIZE)
            block = file[start:end]
            cipher.extend(block_enc(block, self.KEY))
        return cipher

    # Cipher block chaining mode

    def CBC(self, file, block_enc):

        cipher = []
        # put the initial vector in the first 32 bits
        cipher.extend(self.INIT_VEC)

        # break the plaintext into blocks
        # blocks: number of the output's cipher blocks= (plaintext_size/ block_size) /iterations of encryption

        for i in range(ceil(len(file) / self.BLOCK_SIZE + 1)):
            start = i * self.BLOCK_SIZE
            if start >= len(file):
                break
            end = min(len(file), (i+1) * self.BLOCK_SIZE)
            block = file[start:end]
            pre_c = cipher[start:end]  # value of Ci-1
            m = self.xor(block, pre_c)

            cipher.extend(block_enc(m, self.KEY))

        # first 32-bits are initial vector value
        # return cipher from index 32-
        return cipher[self.BLOCK_SIZE:]

    def OFB(self, file, block_enc):
        cipher = []
        keys = []
        # put the initial vector in the first 32 bits
        keys.extend(block_enc(self.INIT_VEC, self.KEY))

        # break the plaintext into blocks
        # blocks: number of the output's cipher blocks= (plaintext_size/ block_size) /iterations of encryption

        for i in range(ceil(len(file) / self.BLOCK_SIZE + 1)):
            start = i * self.BLOCK_SIZE
            if start >= len(file):
                break
            end = min(len(file), (i+1) * self.BLOCK_SIZE)
            P = file[start:end]
            K = keys[start:end]
            C = self.xor(P, K)
            #C = [P[j] ^ K[j] for j in range(len(K))]

            keys.extend(block_enc(K, self.KEY))
            cipher.extend(C)

        # first 32-bits are initial vector value
        # return cipher from index 32-
        return cipher

    def encrypt_file_CBC(self):
        file_name = "encrypted_CBC_"
        file_bin = self.pad_bits(self.file_to_bitslist())
        cipher_list = self.CBC(file_bin, self.encrypt)
        self.bin_to_file(cipher_list, file_name)
        print(
            "Cipher_CBC_vector({}-bits): {}...\n".format(len(cipher_list), cipher_list[:32]))
        # return cipher_list

    def encrypt_file_ECB(self):
        file_name = "encrypted_ECB_"
        file_bin = self.pad_bits(self.file_to_bitslist())
        cipher_list = self.ECB(file_bin, self.encrypt)
        self.bin_to_file(cipher_list, file_name)
        print(
            "Cipher_ECB_vector({}-bits): {}...".format(len(cipher_list), cipher_list[:32]))
        # return cipher_list

    def encrypt_file_OFB(self):
        file_name = "encrypted_OFB_"
        file_bin = self.pad_bits(self.file_to_bitslist())
        cipher_list = self.OFB(file_bin, self.encrypt)
        self.bin_to_file(cipher_list, file_name)
        print(
            "Cipher_OFB_vector({}-bits): {}...".format(len(cipher_list), cipher_list[:32]))
        # return cipher_list
# ========== conversion ===================

    def bitsList_to_str(self, x):
        return "".join(map(str, x))

    # Any file to a binary list
    def file_to_bitslist(self):
        try:
            with open(self.file_name, 'rb') as f:
                bits_list = []
                for byte in bytearray(f.read()):
                    bits_list.append(bin(byte).replace("0b", ""))
                return list(map(int, "".join(bits_list)))

        except FileNotFoundError:
            print("Error: File doesnt exist")

    def bin_to_file(self, bits_list, file_name):
        file_name = file_name + self.file_name
        try:
            bit_string = self.bitsList_to_str(bits_list)
            result = int(bit_string, 2).to_bytes(
                (len(bit_string) + 7) // 8, byteorder='big')
            file = open(file_name, "wb")
            file.write(result)
            file.close()
        except:
            print("Error: in writting the file ")

# ================= oppgave 2========================
        '''
        constructing the key with The Matyas-Meyer-Oseas Construction
        by using the gold function for encryption and generating key i
        ki = g(hi−1)
        hi = ENC(ki, xi) ⊕ xi
        '''

# ----Matyas- Meyer-Oseas construction to construct a hash function----

    def key_construct(self, block, key):
        key = list(map(int, key))
        h = self.xor(self.encrypt(block, key), block)
        next_k = self.pad_bits(h)
        return next_k

    def MMO_construct(self, data, init_key):

        hash_value = []
        init_key = list(map(int, init_key))
        hash_value.extend(init_key)

        for x in range(ceil(len(data) / self.BLOCK_SIZE + 1)):
            start = x * self.BLOCK_SIZE
            if start >= len(data):
                break

            end = min(len(data), (x+1) * self.BLOCK_SIZE)
            block = data[start:end]
            pre_gh = hash_value[start:end]  # value of g(hi-1)
            next_key = self.key_construct(block, pre_gh)
            hash_value.extend(next_key)

        return hash_value

    def encrypt_file_MMO(self):
        file_name = "MMO_" + self.file_name
        file_bin = self.pad_bits(self.file_to_bitslist())
        MMO_construction = list(
            map(int, self.MMO_construct(file_bin, self.INIT_VEC)))
        self.bin_to_file(self.bitsList_to_str(MMO_construction), file_name)
        print("MMO_vector({}-bits): {}...\n".format(len(MMO_construction),
              MMO_construction[:32]))

# ------ hash -----

# --------------------- oppgave 3 -------------------

    def pad_256_bits(self, data):
        return data + [0] * (256 - len(data) % self.BLOCK_SIZE)

    def compute_h(self, key, m):
        bits_list = []
        bin_string = "".join(map(str, key + m))
        hexstr = "{0:0>4X}".format(int(bin_string, 2))
        data = binascii.a2b_hex(hexstr)
        data = sha256(data).digest()
        for byte in bytearray(data):
            bits_list.append(bin(byte).replace("0b", ""))
        bits_list = "".join(bits_list)
        return list(map(int, bits_list))

    def xor_SHA256bit(self, msg, key):
        return self.xor(msg, key)

    def encryption_SHA256(self, l, data, init_vector, init_key):
        cipher = []
        cipher.extend(init_vector)
        for x in range(ceil(len(data) / l + 1)):
            start = x * l
            if start >= len(data):
                break
            end = min(len(data), (x+1) * l)
            block_m = data[start:end]
            pre_m = cipher[start:end]
            h = self.compute_h(init_key, pre_m)
            cipher.extend(self.xor_SHA256bit(h, self.pad_256_bits(block_m)))
        return cipher[len(init_vector):]

    def encrypt_file_SHA256(self):
        file_name = "SHA256_" + self.file_name
        file_bin = self.file_to_bitslist()

        l = self.BLOCK_SIZE  # random l size in our case its just same as others 32 bits
        iv = [1] * 256
        ik = [0, 1] * 128
        SHA256_enc = self.encryption_SHA256(l, file_bin, iv, ik)
        self.bin_to_file(self.bitsList_to_str(SHA256_enc), file_name)
        print("SHA256_vector({}-bits): {}...\n".format(len(SHA256_enc),
              SHA256_enc[:32]))


# ----------------------------------

def main():
    file_name = "gold_plaintext.in"
    gold_enc = Gold(file_name)
    init_vector = gold_enc.bitsList_to_str(gold_enc.INIT_VEC)
    init_key = gold_enc.bitsList_to_str(gold_enc.KEY)

    print("\nInitial_vector({}-bits): {}".format(len(init_vector), init_vector))
    print("Key_vector({}-bits): {}".format(len(init_key), init_key))

    gold_enc.encrypt_file_CBC()
    gold_enc.encrypt_file_ECB()
    gold_enc.encrypt_file_OFB()

    print("Done encrypting the file in CBC, ECB and OFB modes and saved to working directory")

    gold_enc.encrypt_file_MMO()
    print("Done constructing and encrypting with MMO")

    gold_enc.encrypt_file_SHA256()
    print("Done encrypting with SHA256")


if __name__ == "__main__":

    main()
